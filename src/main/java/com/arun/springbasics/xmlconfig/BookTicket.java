package com.arun.springbasics.xmlconfig;

public class BookTicket {

	private String ticketNumber;
	
	private String from;
	
	private String to;
	
	public BookTicket() {
		System.out.println(this.getClass().getSimpleName() +" class object created");
	}
	
	public void bookingConformation() {
		System.out.println("Ticket Booked successfully!!!");
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "BookTicket [ticketNumber=" + ticketNumber + ", from=" + from + ", to=" + to + "]";
	}
	
	
}
